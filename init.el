;;; package --- Summary -*- lexical-binding: t -*-
;;; Commentary:
;;;
;;; My Emacs configuration using org babel

;;; Code:
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))

;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
;; (add-to-list
;;  'package-archives
;;  '("emacswiki" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/emacswiki/") t)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(add-to-list 'load-path "~/.emacs.d/custom/")
(add-to-list 'load-path "~/.emacs.d/custom/helm-slack")
(add-to-list 'load-path "~/.emacs.d/custom/docker-tramp.el/docker-tramp.el")

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)

(use-package org
  :ensure org-contrib)

(org-babel-load-file (expand-file-name "configuration.org" user-emacs-directory))




(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ejc-sql prettier-js expand-region multiple-cursors helm-flycheck flycheck company-lsp pyvenv lsp-ui lsp-mode magit helm-projectile projectile hungry-delete zenburn-theme helm winum use-package org-plus-contrib))
 '(safe-local-variable-values
   '((flycheck-disabled-checkers emacs-lisp-checkdoc)
     (eval let
	   ((env-name "venv"))
	   (unless
	       (equal pyvenv-virtual-env-name env-name)
	     (if
		 (file-directory-p
		  (concat
		   (projectile-project-root)
		   env-name))
		 (pyvenv-activate
		  (concat
		   (projectile-project-root)
		   env-name))
	       (pyvenv-activate "/home/axeo/Projects/venv38")))
	   (unless
	       (bound-and-true-p lsp-mode)
	     (lsp))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(setq package-selected-packages
      '(use-package
	 exec-path-from-shell
	 helm
	 helm-org
	 zenburn-theme
	 zenburn-theme
	 doom-themes
	 all-the-icons
	 modus-vivendi-theme
	 winum
	 projectile
	 ;;  helm-projectile
	 helm-projectile
	 magit
	 forge
	 github-review
	 lsp-mode
	 lsp-pylsp
	 lsp-python-ms
	 lsp-pyright
	 ;; lsp-ui
	 lsp-mode
	 lsp-grammarly
	 pyvenv
	 ;; fira-code-mode
	 company
	 helm-flycheck
	 flycheck
	 flymake
	 paren
	 rainbow-delimiters
	 multiple-cursors
	 expand-region
	 web-mode
	 ;; company-web
	 add-node-modules-path
	 typescript-mode
	 tide
	 ;; js
	 prettier-js
	 ;; ejc-sql
	 vterm
	 doom-modeline
	 minions
	 shackle
	 perspective
	 ;; persp-projectile
	 smartparens
	 tooltip
	 async
	 restclient
	 company-restclient
	 ob-restclient
	 pdf-tools
	 org-bullets
	 ox-jira
	 org-mru-clock
	 org-clock-convenience
	 org-clock-today
	 org-tree-slide
	 highlight-symbol
	 ;;  isortify
	 ;;  pyimpsort
	 py-isort
	 py-yapf
	 sqlformat
	 define-word
	 google-translate
	 ;; omnisharp
	 recentf
	 yaml-mode
	 plantuml-mode
	 tree-sitter
	 tree-sitter-langs
	 go-mode
	 ;; ejira
	 ;; svg-tag-mode
	 which-key
	 frame
	 tab-bar
	 ;; slack
	 helm-slack
	 ;; emacs-everywhere
	 ibuffer
	 posframe
	 company-posframe
	 flycheck-posframe
	 tramp
	 docker-tramp
	 yasnippet
	 ;; marginalia
	 org-roam
	 git-link
	 poporg
	 ob-async
	 solidity-mode
	 vc-msg
	 org-mobile-sync
	 ))

(package-install-selected-packages t)
