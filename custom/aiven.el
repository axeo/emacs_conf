;; Usefull functions for Aiven development workflow

(defun aiven-get-dev-db-dsn ()
  (let* ((json-object-type 'hash-table)
	 (json-array-type 'list)
	 (json-key-type 'string)
	 (json (json-read-file "~/.aiven-test-env.json")))
    (gethash "db" json))
  )

(defun aiven-get-dev-db-password ()
  (let* ((json-object-type 'hash-table)
	 (json-array-type 'list)
	 (json-key-type 'string)
	 (json (json-read-file "~/.aiven-test-env.json")))
    (setq dsn (gethash "db" json))
    (elt (split-string (aiven-get-dev-db-dsn) "@" nil (rx "postgres://avnadmin:")) 0)
    )
  )


(defun aiven-create-dev-ejc-posgres-connection ()
  (interactive)
  ;; All arguments (not only password) should be dynamic and taken from aiven-get-dev-db-dsn
  (progn (rr/create-postgres-connection
	  "aiven-dev"
	  :hostname "aiven-db-aiven-management-axeo-test.avns.net" 
	  :port "20001"
	  :dbname "defaultdb"
	  :username "avnadmin"
	  :password (aiven-get-dev-db-password))
	 )
  )

(defun aiven-run-in-vterm-kill (process event)
  "A process sentinel. Kills PROCESS's buffer if it is live."
  (let ((b (process-buffer process)))
    (and (buffer-live-p b)
         (kill-buffer b))))


(defun aiven-vterm-send-command (command)
  (vterm-send-string command)
  (vterm-send-return))

(defconst toolbox-environments
  '((dev . "dev")
    (prod . "prod"))
  "List of available aiven-core git roots.")

;; TODO: change path and toolbox containter name to defcustom
;; TODO: change aliases AVN-PROD and AAPI depends on enviroment
(defun aiven-open-vterm-with-toolbox ()
  "Open vterm and enter toolbox in chosen environment"
  (interactive)
  (let ((environment (alist-get (intern (completing-read "Select toolbox environment:" toolbox-environments)) toolbox-environments)))
    (with-current-buffer (vterm )
      (set-process-sentinel vterm--process #'aiven-run-in-vterm-kill)
      (message environment)
      
      (if (string-equal environment "dev") (progn
					     (aiven-vterm-send-command "toolbox enter -c aiven-fedora-34")
					     (aiven-vterm-send-command "cd ~/Projects/aiven/aiven-core/")
					     )
	(progn
	  (aiven-vterm-send-command "toolbox enter -c aiven-fedora-34-prod")
	  (aiven-vterm-send-command "cd ~/Projects/aiven/aiven-core-prod/")
	  (aiven-vterm-send-command "AVN-PROD prod checkout-prod")
	  )
	)
      
      (aiven-vterm-send-command "source <(python3 -m aiven.admin dev env)")
      (aiven-vterm-send-command "source <(avn-test dev adminapi-bash-completion --alias AAPI)")
      (aiven-vterm-send-command "source <(avn-test dev zsh-completion)")
      ;; (aiven-vterm-send-command "clear")
      ;; (aiven-vterm-send-command "avn-monitor -1")
      )))

(provide 'aiven)
